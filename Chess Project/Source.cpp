/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project, 
in order to read and write information from and to the Backend
*/

#include "Pipe.h"
#include "Board.h"
#include <iostream>
#include <thread>

//#define STARTINGBOARD "rnbqkbnrpppppppp################################PPPPPPPPRNBQKBNR0"

using std::cout;
using std::endl;
using std::string;


int main()
{
	srand(time_t(NULL));
	Pipe p;
	bool isConnect = p.connect();
	char currentBoard[66] = "rnbqkbnrpppppppp################################PPPPPPPPRNBQKBNR0";
	string ans;
	char msgToGraphics[1024];
	Board board(currentBoard);
	char checkMove[2];
	//declaring vars
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		//if ans == "0" frontend isn't connected
		else 
		{
			p.close();
			return 0;
		}
		//else, frontend is connected
	}
	//checking if frontend is connected

	strcpy_s(msgToGraphics, currentBoard);
	p.sendMessageToGraphics(msgToGraphics);   
	//sending the board string

	string msgFromGraphics = p.getMessageFromGraphics();
	//get message from frontend
	checkMove[0] = board.updateBoard(msgFromGraphics) + '0';
	checkMove[1] = NULL;
	strcpy_s(msgToGraphics, checkMove);
	p.sendMessageToGraphics(msgToGraphics);
	//updating the board for the first time

	while (msgFromGraphics != "quit")
	{
		// should handle the string the sent from graphics
		// according the protocol. Ex: e2e4(move e2 to e4)
		msgFromGraphics = p.getMessageFromGraphics();
		//getting move from frontend
		checkMove[0] = board.updateBoard(msgFromGraphics) + '0';
		checkMove[1] = NULL;
		strcpy_s(msgToGraphics, checkMove);
		//updating the board
		p.sendMessageToGraphics(msgToGraphics);
		//sending checkMove to frontend
	}
	//while user didn't close the game(the frontend)

	p.close();
	//ending the connection
	return 1;
}