#include "King.h"

/*
    Constructor
    Input:
        color, location
    Output:
        None
*/
King::King(string color, int location) :
    Tool("King", color, location)
{

}


/*
    Destructor
    Input:
        None
    Output:
        None
*/
King::~King()
{

}


/*
    Function checks move availability
    Input:
        move from, move to
    Output:
        None
*/
int King::checkMoveAvailablity(string board, int moveTo)
{
    int location = this->getLocation();
    int currentRow = location / 8;
    int currentColumn = location % 8;
    int moveToRow = moveTo / 8;
    int moveToColumn = moveTo % 8;
    if (checkWarning(board, moveTo))
    {
        return 4;
    }
    if ((moveToRow + 1 == currentRow && moveToColumn + 1 == currentColumn)
        || (moveToRow + 1 == currentRow && moveToColumn - 1 == currentColumn)
        || (moveToRow - 1 == currentRow && moveToColumn + 1 == currentColumn)
        || (moveToRow - 1 == currentRow && moveToColumn - 1 == currentColumn)
        || (moveToRow - 1 == currentRow && moveToColumn == currentColumn)
        || (moveToRow + 1 == currentRow && moveToColumn == currentColumn)
        || (moveToColumn - 1 == currentColumn && moveToRow == currentRow)
        || (moveToColumn + 1 == currentColumn && moveToRow == currentRow))
    {
        if (board[moveTo] != '#')
        {
            if (color == "White" && islower(board[moveTo]))
            {
                return 0; // white bishop eats the black tool
            }
            else if (color == "Black" && isupper(board[moveTo]))
            {
                return 0; // black bishop eats the white tool
            }
            return 6;
        }
        return 0; //VALID MOVE
    }
    else
    {
        return 6;
    }
}


bool King::checkWarning(string board, int moveTo)
{
    string color = this->getColor();
    int moveToRow = moveTo / 8;
    int moveToColumn = moveTo % 8;
    char knight = 'n';
    char bishop = 'b';
    char pawn = 'p';
    char queen = 'q';
    char rook = 'r';
    char king = 'k';
    int i = 0;
    bool flag = true;
    int nextSquareLoc;

    //declaring vars  
    if (color == "Black")
    {
        knight = 'N';
        bishop = 'B';
        pawn = 'P';
        queen = 'Q';
        rook = 'R';
        king = 'K';
    }
    if (((((moveToRow + 2) * 8 + (moveToColumn + 1) < 64) && ((moveToRow + 2) * 8 + (moveToColumn + 1) > -1)) && (board[(moveToRow + 2) * 8 + (moveToColumn + 1)] == knight)) || //checking if knight is threating king
        ((((moveToRow + 2) * 8 + (moveToColumn - 1) < 64) && ((moveToRow + 2) * 8 + (moveToColumn - 1) > -1)) && (board[(moveToRow + 2) * 8 + (moveToColumn - 1)] == knight)) ||
        ((((moveToRow + 1) * 8 + (moveToColumn + 2) < 64) && ((moveToRow + 1) * 8 + (moveToColumn + 1) > -1)) && (board[(moveToRow + 1) * 8 + (moveToColumn + 2)] == knight)) ||
        ((((moveToRow + 1) * 8 + (moveToColumn - 2) < 64) && ((moveToRow + 1) * 8 + (moveToColumn - 2) > -1)) && (board[(moveToRow + 1) * 8 + (moveToColumn - 2)] == knight)) ||
        ((((moveToRow - 2) * 8 + (moveToColumn + 1) < 64) && ((moveToRow - 2) * 8 + (moveToColumn + 1) > -1)) && (board[(moveToRow - 2) * 8 + (moveToColumn + 1)] == knight)) ||
        ((((moveToRow - 2) * 8 + (moveToColumn - 1) < 64) && ((moveToRow - 2) * 8 + (moveToColumn - 1) > -1)) && (board[(moveToRow - 2) * 8 + (moveToColumn - 1)] == knight)) ||
        ((((moveToRow - 1) * 8 + (moveToColumn + 2) < 64) && ((moveToRow - 1) * 8 + (moveToColumn + 1) > -1)) && (board[(moveToRow - 1) * 8 + (moveToColumn + 2)] == knight)) ||
        ((((moveToRow - 1) * 8 + (moveToColumn - 2) < 64) && ((moveToRow - 1) * 8 + (moveToColumn - 1) > -1)) && (board[(moveToRow - 1) * 8 + (moveToColumn - 2)] == knight)))
    {
        return true;
    }
    else if ((color == "White") && (((moveToRow - 1) * 8 + (moveToColumn + 1) < 64 && (moveToRow - 1) * 8 + (moveToColumn + 1) > -1 && board[(moveToRow - 1) * 8 + (moveToColumn + 1)] == pawn) || //checking if black pawn is threatning white king
        (moveToRow - 1) * 8 + (moveToColumn - 1) < 64 && (moveToRow - 1) * 8 + (moveToColumn - 1) > -1 && board[(moveToRow - 1) * 8 + (moveToColumn - 1)] == pawn))
    {
        return true;
    }
    else if ((color == "Black") && (((moveToRow + 1) * 8 + (moveToColumn + 1) < 64 && (moveToRow + 1) * 8 + (moveToColumn + 1) > -1 && board[(moveToRow + 1) * 8 + (moveToColumn + 1)] == pawn) ||//checking if white pawn is threatning black king
        (moveToRow + 1) * 8 + (moveToColumn - 1) < 64 && (moveToRow + 1) * 8 + (moveToColumn - 1) > -1 && board[(moveToRow + 1) * 8 + (moveToColumn - 1)] == pawn))
    {
        return true;
    }
    else if ((((moveToRow - 1) * 8 + moveToColumn < 64) && ((moveToRow - 1) * 8 + moveToColumn > -1) && (board[(moveToRow - 1) * 8 + moveToColumn] == king)) || //checking if king is threating king
        (((moveToRow - 1) * 8 + moveToColumn + 1 < 64) && ((moveToRow - 1) * 8 + moveToColumn + 1 > -1) && (board[(moveToRow - 1) * 8 + moveToColumn + 1] == king)) ||
        (((moveToRow - 1) * 8 + moveToColumn - 1 < 64) && ((moveToRow - 1) * 8 + moveToColumn - 1 > -1) && (board[(moveToRow - 1) * 8 + moveToColumn - 1] == king)) ||
        ((moveToRow * 8 + moveToColumn - 1 < 64) && (moveToRow * 8 + moveToColumn - 1 > -1) && (board[moveToRow * 8 + moveToColumn - 1] == king)) ||
        ((moveToRow * 8 + moveToColumn + 1 < 64) && (moveToRow - 2 * 8 + (moveToColumn + 1) > -1) && (board[(moveToRow - 2) * 8 + (moveToColumn + 1)] == king)) ||
        (((moveToRow + 1) * 8 + moveToColumn - 1 < 64) && ((moveToRow + 1) * 8 + moveToColumn - 1 > -1) && (board[(moveToRow + 1) * 8 + moveToColumn - 1] == king)) ||
        (((moveToRow + 1) * 8 + moveToColumn + 1 < 64) && ((moveToRow + 1) * 8 + moveToColumn + 1 > -1) && (board[(moveToRow + 1) * 8 + moveToColumn + 1] == king)) ||
        (((moveToRow + 1) * 8 + moveToColumn < 64) && ((moveToRow + 1) * 8 + moveToColumn > -1) && (board[(moveToRow + 1) * 8 + moveToColumn] == king)))
    {
        return true;
    }
    
    // Checking rook and queen (up and down, left and right)
    for (int i = 0; i < moveTo / 8 && flag; i++) // above
    {
        nextSquareLoc = moveTo - 8 * (i + 1);
        if (board[nextSquareLoc] == queen || board[nextSquareLoc] == rook)
        {
            return true;
        }
        else if (board[nextSquareLoc] != '#')
        {
            flag = false;
        }
    }
    flag = true;

    for (int i = 0; i < 7 - (moveTo / 8) && flag; i++) // below
    {
        nextSquareLoc = moveTo + 8 * (i + 1);
        if (board[nextSquareLoc] == queen || board[nextSquareLoc] == rook)
        {
            return true;
        }
        else if (board[nextSquareLoc] != '#')
        {
            flag = false;
        }
    }
    flag = true;

    for (int i = 0; i < moveTo % 8 && flag; i++) // left
    {
        nextSquareLoc = moveTo -1 * (i + 1);
        if (board[nextSquareLoc] == queen || board[nextSquareLoc] == rook)
        {
            return true;
        }
        else if (board[nextSquareLoc] != '#')
        {
            flag = false;
        }
    }
    flag = true;

    for (int i = 0; i < 7 - (moveTo % 8) && flag; i++) // right
    {
        nextSquareLoc = moveTo + 1 * (i + 1);
        if (board[nextSquareLoc] == queen || board[nextSquareLoc] == rook)
        {
            return true;
        }
        else if (board[nextSquareLoc] != '#')
        {
            flag = false;
        }
    }

    flag = true;
    // Checking bishop and queen (slant right, slant left)
    
    /*
    lineDiffer = 7 - (moveTo % 8) ; right and up
    lineDiffer = (moveTo % 8); right and down 
    lineDiffer = (moveTo % 8); left and up
    lineDiffer = 7 - (moveTo % 8); left and down
     / (jumps * upOrDown)
    */
    for (int i = 0; i < 7 - (moveTo % 8) && flag; i++)
    {
        nextSquareLoc = moveTo + (9 * (i + 1));
        if (nextSquareLoc % 8 == moveTo % 8 + (i + 1) && nextSquareLoc < 64 && nextSquareLoc > -1)
        {
            if (board[nextSquareLoc] == queen || board[nextSquareLoc] == bishop)
            {
                return true;
            }
            else if (board[nextSquareLoc] != '#')
            {
                flag = false;
            }
        }
        else
        {
            flag = false;
        }
    }
    flag = true;
    for (int i = 0; i < 7 - (moveTo % 8) && flag; i++)
    {
        nextSquareLoc = moveTo - (9 * (i + 1));
        if (nextSquareLoc % 8 == moveTo % 8 - (i + 1) && nextSquareLoc < 64 && nextSquareLoc > -1)
        {
            if (board[nextSquareLoc] == queen || board[nextSquareLoc] == bishop)
            {
                return true;
            }
            else if (board[nextSquareLoc] != '#')
            {
                flag = false;
            }
        }
        else
        {
            flag = false;
        }
    }
    flag = true;
    for (int i = 0; i < 7 - (moveTo % 8) && flag; i++)
    {
        nextSquareLoc = moveTo + (7 * (i + 1));
        if (nextSquareLoc % 8 == moveTo % 8 - (i + 1) && nextSquareLoc < 64 && nextSquareLoc > -1)
        {
            if (board[nextSquareLoc] == queen || board[nextSquareLoc] == bishop)
            {
                return true;
            }
            else if (board[nextSquareLoc] != '#')
            {
                flag = false;
            }
        }
        else
        {
            flag = false;
        }
    }
    flag = true;
    for (int i = 0; i < 7 - (moveTo % 8) && flag; i++)
    {
        nextSquareLoc = moveTo - (7 * (i + 1));
        if (nextSquareLoc % 8 == moveTo % 8 + (i + 1) && nextSquareLoc < 64 && nextSquareLoc > -1)
        {
            if (board[nextSquareLoc] == queen || board[nextSquareLoc] == bishop)
            {
                return true;
            }
            else if (board[nextSquareLoc] != '#')
            {
                flag = false;
            }
        }
        else
        {
            flag = false;
        }
    }
    for (int i = 0; i < moveTo % 8 && flag; i++)
    {
        nextSquareLoc = moveTo + (9 * (i + 1));
        if (nextSquareLoc % 8 == moveTo % 8 + (i + 1) && nextSquareLoc < 64 && nextSquareLoc > -1)
        {
            if (board[nextSquareLoc] == queen || board[nextSquareLoc] == bishop)
            {
                return true;
            }
            else if (board[nextSquareLoc] != '#')
            {
                flag = false;
            }
        }
        else
        {
            flag = false;
        }
    }
    flag = true;
    for (int i = 0; i < moveTo % 8 && flag; i++)
    {
        nextSquareLoc = moveTo - (9 * (i + 1));
        if (nextSquareLoc % 8 == moveTo % 8 - (i + 1) && nextSquareLoc < 64 && nextSquareLoc > -1)
        {
            if (board[nextSquareLoc] == queen || board[nextSquareLoc] == bishop)
            {
                return true;
            }
            else if (board[nextSquareLoc] != '#')
            {
                flag = false;
            }
        }
        else
        {
            flag = false;
        }
    }
    flag = true;
    for (int i = 0; i < moveTo % 8 && flag; i++)
    {
        nextSquareLoc = moveTo + (7 * (i + 1));
        if (nextSquareLoc % 8 == moveTo % 8 - (i + 1) && nextSquareLoc < 64 && nextSquareLoc > -1)
        {
            if (board[nextSquareLoc] == queen || board[nextSquareLoc] == bishop)
            {
                return true;
            }
            else if (board[nextSquareLoc] != '#')
            {
                flag = false;
            }
        }
        else
        {
            flag = false;
        }
    }
    flag = true;
    for (int i = 0; i < moveTo % 8 && flag; i++)
    {
        nextSquareLoc = moveTo - (7 * (i + 1));
        if (nextSquareLoc % 8 == moveTo % 8 + (i + 1) && nextSquareLoc < 64 && nextSquareLoc > -1)
        {
            if (board[nextSquareLoc] == queen || board[nextSquareLoc] == bishop)
            {
                return true;
            }
            else if (board[nextSquareLoc] != '#')
            {
                flag = false;
            }
        }
        else
        {
            flag = false;
        }
    }
    return false;
}


int King::checkChess(string board, int moveTo, int moveFrom)
{
    if (board[64] == '0' && isupper(board[moveFrom]) || (board[64] == '1' && islower(board[moveFrom])))
    {
        board[moveTo] = board[moveFrom];
        board[moveFrom] = '#';
    }
    if ((board[moveTo] == 'K' && board[64] == '0') || (board[moveTo] == 'k' && board[64] == '1'))
    {
        if (checkWarning(board, moveTo))
        {
            return 1;
        }
    }
    else if(checkWarning(board, this->getLocation()))
    {
        return 1;
    }
    return 0;
}