#include "Queen.h"

/*
    Constructor
    Input:
        color, location
    Output:
        None
*/
Queen::Queen(string color, int location) :
    Tool("Queen", color, location)
{

}


/*
    Destructor
    Input:
        None
    Output:
        None
*/
Queen::~Queen()
{

}


/*
    Function checks move availability
    Input:
        move from, move to
    Output:
        None
*/
int Queen::checkMoveAvailablity(string board, int moveTo)
{
    int location = this->getLocation();

    //declaring vars
    if (moveTo % 8 == location % 8) //same column
    {
        if (moveTo > location) //rook wants to move down
        {
            return checkMoveFunc(8, board, moveTo);
        }
        else //rook wants to move up
        {
            return checkMoveFunc(-8, board, moveTo);
        }
    }
    else if (moveTo / 8 == location / 8) //same line
    {
        if (moveTo > location) //rook wants to move right
        {
            return checkMoveFunc(1, board, moveTo);
        }
        else //rook wants to move left
        {
            return checkMoveFunc(-1, board, moveTo);
        }
    }
    else if (moveTo > location) //bishop wants to go down
    {
        if (location % 9 == moveTo % 9) //bishop wants to move right and down
        {
            return checkMoveFunc2(1, 1, 9, board, moveTo);
        }
        else if (location % 7 == moveTo % 7) //bishop wants to move left and down
        {
            return checkMoveFunc2(1, -1, 7, board, moveTo);
        }
        else
        {
            return 6; //INVALID MOVE
        }
    }
    else if(moveTo < location) //bishop wants to go up
    {
        if (location % 9 == moveTo % 9) //bishop wants to move left and up
        {
            return checkMoveFunc2(-1, -1, 9, board, moveTo);

        }
        else if (location % 7 == moveTo % 7) //bishop wants to move right and up
        {
            return checkMoveFunc2(-1, 1, 7, board, moveTo);
        }
        else
        {
            return 6; //INVALID MOVE
        }
    }
    else
    {
        return 6; //INVALID MOVE
    }
}


int Queen::checkMoveFunc(int jumps, string board, int moveTo)
{
    int location = this->getLocation();
    string color = this->getColor();
    int lineDiffer = (moveTo - location) / jumps;
    int nextSquareLoc;

    for (int i = 0; i < lineDiffer; i++)
    {
        nextSquareLoc = location + jumps * (i + 1);
        if (board[nextSquareLoc] != '#')
        {
            if (moveTo == nextSquareLoc)//if rook reached the final destination, but there is a tool there, check if you can eat it
            {
                if (color == "White" && islower(board[moveTo]))
                {
                    return 0; // white rook eats the black tool
                }
                else if (color == "Black" && isupper(board[moveTo]))
                {
                    return 0; // black rook eats the white tool
                }
            }
            return 6; //INVALID MOVEMENT, rook moves through another tool
        }
    }
    return 0; //VALID MOVE
}


int Queen::checkMoveFunc2(int upOrDown, int leftOrRight, int jumps, string board, int moveTo)
{
    int location = this->getLocation();
    string color = this->getColor();
    int lineDiffer = (moveTo - location) / (jumps * upOrDown);
    int nextSquareLoc;

    for (int i = 0; i < lineDiffer; i++)
    {
        nextSquareLoc = location + upOrDown * jumps * (i + 1);
        if (nextSquareLoc % 8 == location % 8 + leftOrRight * (i + 1))
        {
            if (board[nextSquareLoc] != '#')
            {
                if (nextSquareLoc == moveTo)
                {
                    if (color == "White" && islower(board[moveTo]))
                    {
                        return 0; // white bishop eats the black tool
                    }
                    else if (color == "Black" && isupper(board[moveTo]))
                    {
                        return 0; // black bishop eats the white tool
                    }
                }
                return 6;
            }
        }
        else
        {
            return 6; //INVALID MOVEMENT
        }
    }
    return 0; //VALID MOVE
}