#include "Rook.h"

/*
    Constructor
    Input:
        color, location
    Output:
        None
*/
Rook::Rook(string color, int location) :
    Tool("Rook", color, location)
{

}


/*
    Destructor
    Input:
        None
    Output:
        None
*/
Rook::~Rook()
{

}



/*
    Function checks move availability
    Input:
        move from, move to
    Output:
        None
*/
int Rook::checkMoveAvailablity(string board, int moveTo)
{
    int location = this->getLocation();

    //declaring vars
    if (moveTo % 8 == location % 8) //same column
    {
        if (moveTo > location) //rook wants to move down
        {
            return checkMoveFunc(8, board, moveTo);
        }
        else //rook wants to move up
        {
            return checkMoveFunc(-8, board, moveTo);
        }
    }
    else if (moveTo / 8 == location / 8) //same line
    {
        if (moveTo > location) //rook wants to move right
        {
            return checkMoveFunc(1, board, moveTo);
        }
        else //rook wants to move left
        {
            return checkMoveFunc(-1, board, moveTo);
        }
    }
    else //INVALID MOVE
    {
        return 6;//INVALID MOVEMENT
    }
}

int Rook::checkMoveFunc(int jumps, string board, int moveTo)
{
    int location = this->getLocation();
    string color = this->getColor();
    int lineDiffer = (moveTo - location) / jumps;
    int nextSquareLoc;

    for (int i = 0; i < lineDiffer; i++)
    {
        nextSquareLoc = location + jumps * (i + 1);
        if (board[nextSquareLoc] != '#')
        {
            if (moveTo == nextSquareLoc)//if rook reached the final destination, but there is a tool there, check if you can eat it
            {
                if (color == "White" && islower(board[moveTo]))
                {
                    return 0; // white rook eats the black tool
                }
                else if (color == "Black" && isupper(board[moveTo]))
                {
                    return 0; // black rook eats the white tool
                }
            }
            return 6; //INVALID MOVEMENT, rook moves through another tool
        }
    }
    return 0; //VALID MOVE
}