#pragma once

#include <iostream>
#include <string>

#include "Tool.h"

using std::string;

#define BOARD_DIMENSIONS 8

class Board
{
	public:
		Board(char* newBoard); //constructor
		~Board();
		void setBoard(char* newBoard); //setter
		char* getBoard(); //getter
		int updateBoard(string move);
		void createTools();
	private:
		char* _currentBoard;
		Tool* _board[BOARD_DIMENSIONS][BOARD_DIMENSIONS];
};

