#pragma once

#include "Tool.h"

class Rook : public Tool
{
public:
    Rook(string color, int location); //constructor
    ~Rook(); //destructor
    virtual int checkMoveAvailablity(string board, int moveTo);
    virtual int checkMoveFunc(int jumps, string board, int moveTo);
};