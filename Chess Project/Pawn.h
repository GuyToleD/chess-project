#pragma once

#include "Tool.h"


class Pawn : public Tool
{
	public:
		Pawn(string color, int location); //constructor
		~Pawn(); //destructor
		virtual int checkMoveAvailablity(string board, int moveTo);
	private:	
		bool _firstMove;
};

