#include "Knight.h"


/*
	Constructor
	Input:
		color, location
	Output:	
		None
*/
Knight::Knight(string color, int location) :
	Tool("Knight", color, location)
{

}


/*
	Destructor
	Input:
		None
	Output:
		None
*/
Knight::~Knight()
{

}


/*
	Function checks move availability
	Input:
		move from, move to
	Output:
		None
*/
int Knight::checkMoveAvailablity(string board, int moveTo)
{
	int location = this->getLocation();
	int currentRow = location / 8;
	int currentColumn = location % 8;
	int moveToRow = moveTo / 8;
	int moveToColumn = moveTo % 8;
	//declaring vars
	if ((moveToRow == currentRow + 2 && moveToColumn == currentColumn + 1 && board[moveTo] == '#') || //knight wants to move down right(2 rows)
		(moveToRow == currentRow + 2 && moveToColumn == currentColumn - 1 && board[moveTo] == '#') || //knight wants to move down left(2 rows)
		(moveToRow == currentRow + 1 && moveToColumn == currentColumn + 2 && board[moveTo] == '#') || //knight wants to move down right(1 row)
		(moveToRow == currentRow + 1 && moveToColumn == currentColumn - 2 && board[moveTo] == '#') || //knight wants to move down left(1 row)
		(moveToRow == currentRow - 2 && moveToColumn == currentColumn + 1 && board[moveTo] == '#') || //knight wants to move up right(2 rows)
		(moveToRow == currentRow - 2 && moveToColumn == currentColumn - 1 && board[moveTo] == '#') || //knight wants to move up left(2 rows)
		(moveToRow == currentRow - 1 && moveToColumn == currentColumn + 2 && board[moveTo] == '#') || //knight wants to move down right(1 row)
		(moveToRow == currentRow - 1 && moveToColumn == currentColumn - 2 && board[moveTo] == '#')) //knight wants to move down left(1 row)
	{
		return 0; //VALID MOVE
	}
	else if ((moveToRow == currentRow + 2 && moveToColumn == currentColumn + 1 && board[moveTo] != '#') || //knight wants to move down right(2 rows)
			 (moveToRow == currentRow + 2 && moveToColumn == currentColumn - 1 && board[moveTo] != '#') || //knight wants to move down left(2 rows)
			 (moveToRow == currentRow + 1 && moveToColumn == currentColumn + 2 && board[moveTo] != '#') || //knight wants to move down right(1 row)
			 (moveToRow == currentRow + 1 && moveToColumn == currentColumn - 2 && board[moveTo] != '#') || //knight wants to move down left(1 row)
	       	 (moveToRow == currentRow - 2 && moveToColumn == currentColumn + 1 && board[moveTo] != '#') || //knight wants to move up right(2 rows)
	 		 (moveToRow == currentRow - 2 && moveToColumn == currentColumn - 1 && board[moveTo] != '#') || //knight wants to move up left(2 rows)
			 (moveToRow == currentRow - 1 && moveToColumn == currentColumn + 2 && board[moveTo] != '#') || //knight wants to move down right(1 row)
			 (moveToRow == currentRow - 1 && moveToColumn == currentColumn - 2 && board[moveTo] != '#')) //knight wants to move down left(1 row)
	{
		if (color == "White" && islower(board[moveTo]))
		{
			return 0; // white bishop eats the black tool
		}
		else if (color == "Black" && isupper(board[moveTo]))
		{
			return 0; // black bishop eats the white tool
		}
		else
		{
			return 6; //INVALID MOVE
		}
	}
	else
	{
		return 6; //INVALID MOVE
	}
}