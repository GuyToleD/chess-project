#pragma once

#include "Tool.h"

class Bishop : public Tool
{
	public:
		Bishop(string color, int location); //constructor
		~Bishop(); //destructor
		virtual int checkMoveAvailablity(string board, int moveTo);
		virtual int checkMoveFunc(int upOrDown, int leftOrRight, int jumps, string board, int moveTo);
};

