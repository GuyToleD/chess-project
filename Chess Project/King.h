#pragma once

#include "Tool.h"


class King : public Tool
{
public:
    King(string color, int location); //constructor
    ~King(); //destructor
    virtual int checkMoveAvailablity(string board, int moveTo);
    bool checkWarning(string board, int moveTo);
    int checkChess(string board, int moveTo, int moveFrom);
};