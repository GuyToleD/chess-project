#pragma once

#include "Tool.h"


class Queen : public Tool
{
public:
    Queen(string color, int location); //constructor
    ~Queen(); //destructor
    virtual int checkMoveAvailablity(string board, int moveTo);
    int checkMoveFunc(int jumps, string board, int moveTo);
    int checkMoveFunc2(int upOrDown, int leftOrRight, int jumps, string board, int moveTo);
};